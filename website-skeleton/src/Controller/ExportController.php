<?php

namespace App\Controller;

use App\Entity\Post;
use App\Services\ExportService;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\BinaryFileResponse;
use Symfony\Component\Routing\Annotation\Route;

class ExportController extends AbstractController
{
    /**
     * @Route("/export", name="export")
     * @param ExportService $exportService
     * @return BinaryFileResponse
     */

    public function export(ExportService $exportService)
    {
        $exportService->exportPost();

        return new BinaryFileResponse('export.csv');
    }

    /**
     * @Route("/import")
     */
    public function import()
    {
        $em = $this->getDoctrine()->getManager();

        if (($handle = fopen("export.csv", "r")) !== FALSE) {
            while (($data = fgetcsv($handle, 1000, ",")) !== FALSE)
            {
                $post = new Post();
                $post->setTitre($data[1]);
                $post->setContenu($data[2]);

                $em->persist($post);
                $em->flush();
            }
            fclose($handle);
        }
        die;
    }
}
