<?php

namespace App\Controller;

use App\Entity\Post;
use App\Form\PostType;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpKernel\Exception\BadRequestHttpException;
use Symfony\Component\Routing\Annotation\Route;


/**
 * class ApiController
 * @package App\Controller
 * @Route("/api", name="api")
 */
class ApiController extends AbstractController
{
    /**
     * @Route("/post", methods={"GET"})
     */
    public function getPost()
    {
        /** @var Post[] $post */
        $post = $this->getDoctrine()->getManager()->getRepository(Post::class)->findAll();

        $response = [];

        foreach($post as $value){
            $response[] = $value->toArray();
        }

        return new JsonResponse($response);
    }

     /**
     * @Route("/post", methods={"POST"})
     * @param Request $request
     * @return JsonResponse
     */
    public function postPost(Request $request)
    {
        $post = new Post();
        $form = $this->createForm(PostType::class, $post);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $entityManager = $this->getDoctrine()->getManager();
            $entityManager->persist($post);
            $entityManager->flush();

            return new JsonResponse("ok");
        }
        throw new BadRequestHttpException();
    }

     /**
     * @Route("/packagist", methods={"GET"})
     * @param Request $request
     * @return JsonResponse
     */
    public function packagist(Request $request)
    {
        $query = $request->get('search', '');

        $result = json_decode(file_get_contents('https://packagist.org/search.json?q='.$query), true);

        return new JsonResponse($result['results']);
    }

}
