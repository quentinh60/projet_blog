<?php

namespace App\Services;

use App\Entity\Post;
use Doctrine\ORM\EntityManagerInterface;

class ExportService
{
    /**
     * @var EntityManagerInterface
     */
    private $em;

    public function __construct(EntityManagerInterface $em)
    {
        $this->em = $em;
    }

    /**
     * exporte la liste des Post dans un fichier .csv
     */
    public function exportPost()
    {
        /** @var Post[] $list */
        $post = $this->em->getRepository(Post::class)->findAll();

        $fp = fopen('export.csv', 'w');

        foreach ($post as $fields) {
            fputcsv($fp, $fields->toArray());
        }

        fclose($fp);
    }
}

